package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class User(
    val id:String,
    val nom:String,
    val sexualidad:String
)