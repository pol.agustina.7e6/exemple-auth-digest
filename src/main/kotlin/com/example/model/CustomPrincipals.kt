package com.example.model

import io.ktor.server.auth.*
import java.security.MessageDigest
import kotlin.text.Charsets.UTF_8

data class CustomPrincipal(
    val userName: String,
    val realm: String
):Principal

fun getMd5Digest(str: String): ByteArray = MessageDigest.getInstance("MD5").digest(str.toByteArray(UTF_8))

val myRealm = "Access to the '/' path"
val userTable: Map<String, ByteArray> = mapOf(
    "admin" to getMd5Digest("admin:$myRealm:admin")
)
