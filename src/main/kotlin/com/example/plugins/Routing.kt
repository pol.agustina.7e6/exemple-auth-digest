package com.example.plugins

import com.example.model.User
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File

fun Application.configureRouting() {

    val users = Json.decodeFromString<List<User>>(File("./users.json").readText())

    routing {
        authenticate("auth-digest"){
            route("/users"){
                get{
                    call.respond(users)
                }
            }
        }
    }
}
